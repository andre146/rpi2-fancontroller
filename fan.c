#include <stdio.h>
#include <wiringPi.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>

#define TEMPFILE_PATH "/sys/class/thermal/thermal_zone0/temp" //where to get the temp from
#define FAN_PIN 1 //default pin
#define PWM_RANGE 1024 //default pwm range
#define SLEEP_TIME 1
#define CONFIGFILE_PATH "fan.conf"
#define DEFAULT_PROP_GAIN 1
#define DEFAULT_INT_GAIN 0
#define DEFAULT_CUTOFF 20
#define OVERTEMP_THRES 5
#define DEFAULT_LOG_INTERVAL 60

unsigned char fanPin = FAN_PIN;
int setTemp;

float getTemp(){ //reads the temperature and returns it in degree celsius

	FILE *tempFile = fopen(TEMPFILE_PATH, "r"); //open the file
	long fileLen;

	if(tempFile == NULL){ //check if opened correctly
                return(-1);
        }

	fseek(tempFile, 0, SEEK_END); //go to the end
	fileLen = ftell(tempFile) + 1;//get the length
	fseek(tempFile, 0, SEEK_SET);//go to the start
	char buffer[fileLen + 1];

	if(buffer == NULL){
	  fclose(tempFile);
	  return(-1);
	}

	fgets(buffer, fileLen, tempFile);
	fclose(tempFile);

	return((float)atof(buffer) / 1000);

}

void cleanup(){ //resets the gpio pins
	pwmWrite(fanPin, 0);
	pinMode(fanPin, INPUT);
	printf("Exit...\n");
	exit(0);
}

void clearSeize(unsigned char pin){
	pwmWrite(pin, 0);
	sleep(2);
	pwmWrite(pin, PWM_RANGE);
	sleep(2);
	pwmWrite(pin, 0);
	sleep(1);
	pwmWrite(pin, PWM_RANGE);
}

void monitor(char enable_log, char *logfile_name){
	static char overtemp = 0;
	static char file_perm_set = 0;
	float cpu_temp = getTemp();
	char stringBuffer[25];
	FILE *logFile;	
	time_t currTime = time(NULL);
	struct tm ltime = *localtime(&currTime);

	if(overtemp && cpu_temp >= (setTemp + OVERTEMP_THRES)){
		clearSeize(fanPin);
	}else if(cpu_temp >= (setTemp + OVERTEMP_THRES)){
		overtemp = 1;
	}else{
		overtemp = 0;
	}

	if(enable_log){
		logFile = fopen(logfile_name, "a+");
		if(!file_perm_set){
			chmod(logfile_name, S_IRUSR | S_IWUSR | S_IXUSR | S_IROTH | S_IWOTH | S_IXOTH);
			file_perm_set = 1;
		}
		
		sprintf(stringBuffer, "%d-%d-%d %d:%d,%.3f\n", ltime.tm_year + 1900, ltime.tm_mon + 1, ltime.tm_mday, ltime.tm_hour, 			ltime.tm_min, cpu_temp);

		fwrite(stringBuffer, strlen(stringBuffer), 1, logFile);	
		fclose(logFile);
	}
	
}

int main(int argc, char **argv){

	if(argc <= 1){ //one argument is required which is the set temperatue
	  printf("Usage: <temp>\n");
	  return(1);
	}
	if(geteuid() != 0){ //check if run with root privileges. Running as a normal user leads to a system crash!
	  printf("This program must be run as root!\n");
	  return(1);
	}

	/*declaring and initializing some variables*/
	
	setTemp = atoi(argv[1]);
	int cutoff = 0; //cutoff point at which the fan does not turn 
	int effort = 0; // the controller output
	char startup = 0;
	char *logFileName; //contains the name of the logfile
	int logInterval = DEFAULT_LOG_INTERVAL;
	float output = 0; //The controller output in percent
	float currTemp = 0; //The current CPU temperature
	float error = 0; // error, in this case actual value - setpoint
	float errorSum = 0; // error sum for integration
	float tmpErrorSum = 0; //Temporary storage for the last error sum that stays inside the PWM value bounds	
	float propEffort = 0; // proportional output
	float intEffort = 0; // integral output
	float propGain = DEFAULT_PROP_GAIN; // the proportinal controller gain
	float intGain = DEFAULT_INT_GAIN; // the integral controller gain
	int loopTime = 0;
	int enable_log = 0;

	printf("----------Raspberry Pi fan controller by Andre Picker----------\n");

	/*
	Now follows the parsing of the config file
	*/

	FILE *confFile = fopen(CONFIGFILE_PATH, "rb"); 
	long fileLen = 0;
	long filePos = 0;
	char *lineBuffer; //later holds one single line
	char *cmdBuffer; //contains the variable to be set
	char *argBuffer; //contains the value
	
	if(confFile == NULL){
	  printf("Failed to open config file %s!\n", CONFIGFILE_PATH);
		return(1);
	}

	fseek(confFile, 0, SEEK_END); //getting file length
	fileLen = ftell(confFile) + 1;
	fseek(confFile, 0, SEEK_SET);
	lineBuffer = malloc(fileLen + 1); //allocating enough memory
//	logFileName = malloc(fileLen + 1);

	while(filePos < fileLen - 1){ //walking through the file line by line by utilising fgets() which stops at \n
		fgets(lineBuffer, fileLen, confFile);
		filePos = ftell(confFile);

		if(lineBuffer != NULL && lineBuffer[0] != '#'){ //ignores line if it is a comment or NULL
			cmdBuffer = strtok(lineBuffer, " "); // split at space character
			argBuffer = strtok(NULL, "\n");
			
			if(argBuffer == NULL || cmdBuffer == NULL){
				continue;
			}
			if(strcmp(cmdBuffer, "log_interval") == 0){ //switch depending on variable
				logInterval = atoi(argBuffer);
			}else if (strcmp(cmdBuffer, "fanPin") == 0){
				fanPin = atoi(argBuffer);
			}else if (strcmp(cmdBuffer, "propGain") == 0){
                         	propGain = (float)atof(argBuffer);
                        }else if (strcmp(cmdBuffer, "intGain") == 0){
                                intGain = (float)atof(argBuffer);
                        }else if(strcmp(cmdBuffer, "cutoff") == 0){
				cutoff = atoi(argBuffer);
			}else if(strcmp(cmdBuffer, "logfile") == 0){
				logFileName = malloc(strlen(argBuffer) + 1);
				strcpy(logFileName, argBuffer);
			}else if(strcmp(cmdBuffer, "enable_log") == 0){
				enable_log = (char)atoi(argBuffer);
			}else{
				printf("Unknown argument '%s' in config file!\n", cmdBuffer);
				fclose(confFile);
				return(1);
			}
		}
	}

//	free(lineBuffer); //does not work for some reason
	fclose(confFile);

	signal(SIGINT, cleanup); //initializing a signal handler to call cleanup() when an interrupt signal is sent

        wiringPiSetup(); //setting up the wiringPi lib
        pwmSetRange(PWM_RANGE);
//	pwmSetClock(3840); //does this even work at all?
        pwmSetMode(PWM_MODE_MS);
	pinMode(fanPin, PWM_OUTPUT);

	printf("Fan Pin: %i\n", fanPin);
	printf("Proportional gain: %.4f\n", propGain);
	printf("Integral gain: %.4f\n", intGain);
	printf("Cutoff point: %i\n", cutoff);
	printf("Log enabled: %s\n", enable_log ? "yes" : "no");
	printf("Log interval: %i Seconds\n", enable_log ? logInterval : 0);
	printf("Set Temperature: %i°C\n\n", setTemp);

	while(1){
		
		currTemp = getTemp();
		error = currTemp - setTemp;
		errorSum += error;

		propEffort = propGain * error;
		intEffort = intGain * errorSum * SLEEP_TIME;
		effort = (int)round(propEffort + intEffort);

		if(effort > PWM_RANGE){ //clamp the effort to the range of the pwm value
			effort = PWM_RANGE;
			errorSum = tmpErrorSum;
		} else if(effort <= 0){
			effort = 0;
			startup = 1;
			errorSum = tmpErrorSum;
		}else{
			tmpErrorSum = errorSum;
		}
		if(startup && effort > cutoff){
			pwmWrite(fanPin, PWM_RANGE);
			sleep(SLEEP_TIME);
			startup = 0;
		}
		if(effort <= cutoff){
			effort = 0;
			startup = 1;
		}	
		
		output = (float)effort / PWM_RANGE * 100;
		pwmWrite(fanPin, effort);
		printf("\rOutput: %.3f%%   Temp: %.3f°C    ", output, currTemp);
		fflush(stdout);

		if(loopTime >= logInterval){
			monitor(enable_log, logFileName);
			loopTime = 0;
		}

		sleep(SLEEP_TIME);
		loopTime += SLEEP_TIME;
	}

	return 0;
}
